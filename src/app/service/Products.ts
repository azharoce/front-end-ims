export class Products {
    _id!: String;
    product_id!: Number;
    product_name!: String;
    product_gtin!: String;
    product_price!: number;
    product_sell_price!: number;
    product_discrount!: number;
    product_flag!: number;
    product_dynamic_link!: String;
    product_brand!: String;
    product_type!: String;
    product_colour!: String;
    product_tag!: String;
    product_images!: String;
    description!: String;
    publish_date!: Date;
}